# Grails/Spring Security UI Plug-in Bug

There seems to be a possible bug in the Spring Security UI plug-in.  Step to reproduce:

1. Create a new Grails application (4.0.2 in this example)
1. Add dependencies for Spring Security Core and UI
1. run the s2-quickstart script to create domain objects
1. add code to `BootStrap.groovy` to create an admin user if none exists
1. update `application.groovy` to grant the admin user access
1. start the system and via the security UI create at least two roles
1. via the UI create a new user and assign more than one role
1. via the UI edit the user and remove just one of the roles

The result is that all roles are removed from the user.