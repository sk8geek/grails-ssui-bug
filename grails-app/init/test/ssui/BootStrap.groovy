package test.ssui

class BootStrap {

    def init = { servletContext ->

        if (!User.findByUsername("admin")) {
            User.withTransaction {
                def admin = new User(username: "admin", password: "password").save()
                def role = new Role(authority: "ROLE_ADMIN").save()
                new UserRole(user: admin, role: role).save(flush: true)
            }
        }

    }
    def destroy = {
    }
}
